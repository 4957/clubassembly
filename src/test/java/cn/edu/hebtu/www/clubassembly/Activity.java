package cn.edu.hebtu.www.clubassembly;

/**
 * Created by root on 16-3-14.
 */
public class Activity {
    private String pn;
    private String an;
    private String aid;
    private String time_begin;
    private String time_finish;
    private String number_start;
    private String number_end;
    private String count;
    private String status;
    private String pic;

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getAn() {
        return an;
    }

    public void setAn(String an) {
        this.an = an;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getTime_begin() {
        return time_begin;
    }

    public void setTime_begin(String time_begin) {
        this.time_begin = time_begin;
    }

    public String getTime_finish() {
        return time_finish;
    }

    public void setTime_finish(String time_finish) {
        this.time_finish = time_finish;
    }

    public String getNumber_start() {
        return number_start;
    }

    public void setNumber_start(String number_start) {
        this.number_start = number_start;
    }

    public String getNumber_end() {
        return number_end;
    }

    public void setNumber_end(String number_end) {
        this.number_end = number_end;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    /* "pn": "健康理疗体验预约",
            "an": "",
            "aid": "A6F3D839-1AE7-9C2F-DE02-F6AD8600898D",
            "time_begin": "2016-02-21 20:00:00",
            "time_finish": "2016-02-28 17:00:00",
            "number_start": "1",
            "number_end": "50",
            "count": "50",
            "status": "1",
            "pic": "upload/19A8C354/BB1A/F1E5/B502/84EF574D6794/21456054247.png"*/
}
