package cn.edu.hebtu.www.clubassembly;

import java.util.List;

/**
 * Created by root on 16-3-12.
 */
public class CommonResponse {
    private int c;
    private String m;
    private List<String> codes;
    private String responseStr;

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public List<String> getCodes() {
        return codes;
    }
    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }
}
